#!/usr/bin/env bats

cmd=./aports-lint
apkbuild=$BATS_TMPDIR/APKBUILD

# Remove APKBUILD_STYLE variable from the environment as that can affect results
unset APKBUILD_STYLE

# Skip AL58 as it can't find main/db and other custom deprecated packages
export SKIP_DEPRECATED_PACKAGES=YES

assert_match() {
	output=$1
	expected=$2

	echo "$output" | grep -qE "$expected"
}

is_travis() {
	test -n "$TRAVIS"
}

retab() {
	case $(realpath "$(which unexpand)") in
		*busybox*) flag="-f";;
		*) flag="--first-only";;
	esac

	unexpand $flag -t4
}

@test 'Local patch has description' {
	cat <<-__EOF__ > $apkbuild
		source="https://github.com/user/repo/archive/$pkgver.tar.gz
			fix-build.patch
			"
	__EOF__

	cat <<-__EOF__ > $BATS_TMPDIR/fix-build.patch
		Upstream: Should be
		Reason: Fixes build with musl by including limits.h for PATH_MAX

		--- libreswan-3.27.orig/programs/pluto/connections.c
		+++ libreswan-3.27/programs/pluto/connections.c
		@@ -41,6 +41,7 @@
		 #include <arpa/inet.h>
		 #include <resolv.h>
		 #include <errno.h>
		+#include <limits.h>

		 #include <libreswan.h>
		 #include "libreswan/pfkeyv2.h"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'Local patch has no description' {
	cat <<-__EOF__ > $apkbuild
		source="https://github.com/user/repo/archive/$pkgver.tar.gz
			fix-build.patch
			"
	__EOF__

	cat <<-__EOF__ > $BATS_TMPDIR/fix-build.patch
		--- libreswan-3.27.orig/programs/pluto/connections.c
		+++ libreswan-3.27/programs/pluto/connections.c
		@@ -41,6 +41,7 @@
		 #include <arpa/inet.h>
		 #include <resolv.h>
		 #include <errno.h>
		+#include <limits.h>

		 #include <libreswan.h>
		 #include "libreswan/pfkeyv2.h"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "\[AL56\].*:Patch file ./fix-build.patch is missing a description"
}
